# Laboratorio 1 - Paradigmas 2018

## Programación Orientada a Objetos con Ruby

### Requerimientos
* Ruby version 2.5.0  
* sqlite3
* bundler

### Correr el servidor

Primero instalar las gemas de Gemfile usando  
`$ bundle`  
Luego ejecutar:  
`$ bundle exec rake db:migrate `  
`$ bundle exec rake ` para levantar el server  

### Decisiones de diseño
La primera gran decisión fue implementar la base de datos desde un principio.  
En este lab utilizamos _Active Record_ para manejar las instancias que se  
van a guardar en la base de datos.  

#### Problema 1: Single table inheritance
Dado que los **Consumers** y los **Providers** tienen muchas cosas en común,  
decidimos que lo mejor era tener una clase madre, **User** de la que hereden.  
Para lograr esto en una base de datos hay dos maneras posibles de hacerlo,  
[una tabla](http://api.rubyonrails.org/classes/ActiveRecord/Inheritance.html ) con la unión de los atributos de los dos modelos, o dos tablas  
separadas donde se repiten los atributos en común de los modelos.  
Como queríamos mantener el concepto de herencia lo más fiel al dominio de  
las clases se implementó con una sola tabla.  
Esta decisión explica el siguiente código
```ruby
belongs_to :consumer, class_name: "Consumer"
belongs_to :provider, class_name: "Provider"
```
Esto se debe a que **Order** se relaciona con un **Consumer** y un   
**Provider** que como están en la misma tabla hay que relacionarlos  
a través de ` class_name `.

#### Problema 2: Lack of attr_{reader, writer, accessor}  
[Resumiendo](https://codedecoder.wordpress.com/2013/01/19/accessors-attr_reader-attr_writer-attr_accessor-in-ruby/)
las clases que heredan de ` ActiveRecord::Base `  no usan  
` attr_r|w|a ` como helpers methods sino que tienen sus propios  
helpers e.g. ` attr_accessible ` por lo que se trató de mantener el  
**encapsulamiento** definiendo métodos *privados* de clase como en la  
clase **Consumer**
```ruby
private
def self.readable_attributes
  ["id", "email", "wallet", "location_id"]
end
```  
Cómo hacemos uso de esto?  
Dado que usamos una api RESTful y se hace mucho uso del formato  
_json_ se redefine el método ` as_json ` de las clases que heredan de   
` ActiveRecord::Base ` de la forma
```ruby
def as_json(options = {})
    options[:only] ||= self.class.readable_attributes
    super_json = super(options)
    super_json["location"] = super_json.delete "location_id"
    super_json
  end
```
donde el objeto instanciado llama al método _privado_ de la clase para saber cúales  
son los atributos que realmente puede devolver.  
NOTA: ` to_json ` llama a ` as_json `

Pero por qué no usar
```ruby
to_json(method: :location)
```  
para settear la location de un **User**?  
El motivo es que ` User.location ` ya está implementado por la relación  
entre **User** y **Location** y es muy útil para relacionar objetos por lo tanto  
es un trade off muy grande.
