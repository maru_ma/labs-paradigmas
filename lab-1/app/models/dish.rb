require 'sinatra/activerecord'


class Dish < ActiveRecord::Base
  belongs_to :provider
  has_and_belongs_to_many :orders

  def as_json(options = {})
    super_json = super(options)
    super_json["provider"] = super_json.delete "provider_id"
    super_json
  end
end
