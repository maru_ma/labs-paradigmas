require 'json'
require 'sinatra/base'
require 'sinatra/json'
require 'sinatra/namespace'
require 'sinatra'
require 'sinatra/activerecord'
require './models/user'
require './models/location'
require './models/order'
require './models/dish'

configure do
  # Config our Database, in our case is a simple file-based SQLITE

  ActiveRecord::Base.establish_connection(
    :adapter => "sqlite3",
    :database => "deliveru.sqlite3"
  )

  set :show_exceptions, true
end


# Main class of the application
class DeliveruApp < Sinatra::Application
  register Sinatra::ActiveRecordExtension
  register Sinatra::Namespace

  enable :sessions unless test?

  ## Function to clean up the json requests.
  before do
    begin
      if request.body.read(1)
        request.body.rewind
        @request_payload = JSON.parse(request.body.read,
                                        symbolize_names: true)
      end
    rescue JSON::ParserError
      request.body.rewind
      puts "The body #{request.body.read} was not JSON"
    end
  end

  register do
    def auth
      condition do
        halt 401 unless session.key?(:logged_id) || self.settings.test?
      end
    end
  end

  ## API functions
  namespace '/api' do

    post '/login' do
      user = User.find_by_email @request_payload[:email]
      if user.nil?
        [401, "Non existing user"]
      elsif user.password == @request_payload[:password]
        session[:logged_id] = user.id
        isProvider = user.is_a?(Provider)
        [200, {id: user.id, isProvider: isProvider}.to_json]
      else
        [403, "Incorrect password"]
      end
    end

    post '/logout' do
      session.delete(:logged_id)
      200
    end

    post '/providers' do
      payload = @request_payload.reject {|key, val| val.nil?}
      halt 400 unless payload.include?(:email) &&
                      payload.include?(:location)
      new_provider = Provider.find_by_email @request_payload[:email]
      if new_provider
        [409, "Existing Provider"]
      else
        new_provider = Provider.new()
        new_provider.email = @request_payload[:email]
        new_provider.password = @request_payload[:password]
        new_provider.store_name = @request_payload[:store_name]
        new_provider.location_id = @request_payload[:location]
        if new_provider.valid?
          new_provider.save
          [200, new_provider.id.to_s]
        else
          [400, new_provider.errors.full_messages]
        end
      end
    end

    post '/items' do
      payload = @request_payload.reject {|key, val| val.nil?}
      exists_keys = payload.include?(:name) &&
                    payload.include?(:price) &&
                    payload.include?(:provider)
      halt 400 unless exists_keys
      provider = Provider.find_by_id @request_payload[:provider]
      halt 404 unless provider
      halt 409 if provider.dishes.find_by_name @request_payload[:name]
      dish = Dish.new
      dish.name = @request_payload[:name]
      dish.price = Float(@request_payload[:price]) rescue nil
      dish.provider = provider
      dish.save
      [200, dish.id.to_s]
    end

    post '/items/delete/:id' do |id|
      item = Dish.find_by_id id
      halt [404, "Non existing item"] unless item
      halt [403, "Item does not belong to logged provider"] unless
                                                item.provider_id == session[:logged_id]
      item.delete
      200
    end

    get '/providers' do
      if params["location"].nil?
        [200, Provider.all.as_son(only: [:id, :email,
                                    :store_name, :location_id])]
      else
        locations = Location.find_by_id params["location"]
        if  locations.nil?
          [404, "Non existing location"]
        else
          [200, Provider.where(location_id: locations.id).to_json(only: [:id,
                                                        :email, :store_name,
                                                        :location_id])]
        end
      end
    end

    get '/consumers' do
      res = [200, Consumer.all.to_json(only: [:id, :email,
                                            :location_id])]
      res
    end

    post '/consumers' do
      payload = @request_payload.reject {|key, val| val.nil?}
      halt 400 unless payload.include?(:email) &&
                      payload.include?(:location)
      user = User.find_by_email payload[:email]
      halt 409 if user
      user = Consumer.new
      user.email = payload[:email]
      location = Location.find_by_id payload[:location]
      user.location = location
      user.password = payload[:password]
      if user.valid?
        user.save
        [200, user.id.to_s]
      else
        [400, user.errors.full_messages]
      end
    end

    post '/users/delete/:id' do |id|
      user = User.find_by_id id
      halt [404, "Non existing user"] unless user
      user.delete
      200
    end

    get '/items' do
      if params["provider"].nil?
        [200, Dish.all.to_json]
      else
        provider = Provider.find_by_id params["provider"]
        halt [400, "Non existing provider"] unless provider
        [200, Dish.where(provider: provider).to_json]
      end
    end

    post '/orders' do
      payload = @request_payload.reject {|key, val| val.nil?}
      [400, "Incomplete information"] unless payload.include?(:provider) &&
                                              payload.include?(:items) &&
                                              payload.include?(:consumer)
      provider = Provider.find_by_id payload[:provider]
      [404, "No existing provider"] unless provider
      consumer = Consumer.find_by_id payload[:consumer]
      [404, "No existing consumer"] unless consumer
      items = payload[:items]
      [404, "No existing item"] unless items
      new_order = Order.new()
      new_order.consumer = consumer
      new_order.provider = provider
      new_order.price = 0.0
      dishes_array = []
      if consumer.wallet && provider.wallet
        items.each do |item|
          dish = Dish.find_by_id item[:id]
          for i in 1..item[:amount] do
            new_order.price += dish.price
            consumer.wallet -= dish.price
            if new_order.price > consumer.wallet
              halt 404
            else
              dishes_array << dish
              provider.wallet += dish.price
            end
          end
        end
        new_order.dishes = dishes_array
        new_order.save
        [200, new_order.id.to_s]
      end
    end

    get '/orders/detail/:id' do |id|
      halt 400 unless id
      order = Order.find_by_id id
      if order
        res = []
        items = order.dishes.group(:id)
        amount_per_id = items.count
        items.each do |i|
          item_hash = i.as_json(only: ["id", "name", "price"])
          item_hash["amount"] = amount_per_id[item_hash["id"]]
          price = item_hash["price"].to_f
          item_hash.delete "price"
          item_hash["price"] = price
          res << item_hash
        end
        [200, res.to_json]
      else
        halt 404
      end
    end

    get '/orders' do
      halt 400 unless params["user_id"]
      user = User.find_by_id params["user_id"]
      halt 404 unless user
      res = []
      user_orders = user.orders
      user_orders.each do |ord|
        res << ord.as_json
      end
      [200, res.to_json]
    end

    post '/orders/delete/:id' do |id|
      ord = Order.find_by_id id
      ord.destroy if ord
      200
    end

    post '/deliver/:id' do |id|
      order = Order.find_by_id id
      halt [404, "Non existing order"] unless order
      order.delivered!
      200
    end

    get '/users/' do
      halt 400
    end

    get '/users/:id' do |id|
      halt [400, "No user id"] unless id
      user = User.find_by_id id
      halt [404, "Non existing user"] unless user
      if user.is_a?(Provider)
        [200, user.to_json(only: [:id, :email, :wallet, :business_name])]
      else
        [200, user.to_json(only: [:id, :email, :wallet])]
      end
    end

    get '/locations' do
      [200, Location.all.to_json()]
    end

    get '*' do
      halt 404
    end
  end

  # This goes last as it is a catch all to redirect to the React Router
  get '/*' do
    erb :index
  end
end
