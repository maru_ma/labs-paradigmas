class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password
      t.decimal :wallet, default: 0.0
      t.string :store_name
      t.string :type

      t.belongs_to :location
    end
  end
end
