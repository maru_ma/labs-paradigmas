class CreateOrder < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.column :status, :integer, default: 0
      t.decimal :price, default: 0.0

      t.integer :consumer_id
      t.integer :provider_id
    end
  end
end
