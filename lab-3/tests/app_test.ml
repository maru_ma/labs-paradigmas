open App
open OUnit2

open GraphicsTest

(* Los tests funcionan del siguiente modo: la función `load` toma dos listas. La
   primera contiene los eventos (mouse, teclado), la segunda las acciones (sobre
   Graphics) que se esperan. Por ejemplo:

     load [key 'e'] [Open]

   Toma un único evento (la tecla 'e'), y espera una única acción (abrir la ventana).
   En el módulo `graphicsTest` pueden ver todos los eventos y las acciones.
*)
module TestApp = App(G)
open G

(* Definimos algunos eventos *)
(* keys *)
let ke = key 'e'
let kc = key 'c'
let kr = key 'r'
let kd = key 'd'

(* clicks *)
let p1 = mouse (5, 10)
let p2 = mouse (9, 11)

(* Definimos unas acciones *)
(* rectangles *)
let r1 = (0, 5, 10, 10)
let r2 = (4, 6, 10, 10)

(* circles *)
let c1 = (5, 10, 5)
let c2 = (9, 11, 5)

let op = Open
let wc = DrawString "waiting for circle"
let wr = DrawString "waiting for rectangle"
let wd = DrawString "waiting for deletion"
let fc1 = FillCircle c1
let fc2 = FillCircle c2
let fr1 = FillRect r1
let fr2 = FillRect r2
let cl = Clear

let exit _ =
  let open G in
  G.load [ke] [op];
  TestApp.start ();
  G.close ()

let circle _ =
  let open G in
  G.load [kc; p1; ke] [op; wc; fc1];
  TestApp.start ();
  G.close ()

let rectangle _ =
  let open G in
  G.load [kr; p1; ke] [op; wr; fr1];
  TestApp.start ();
  G.close ()

let two_rectangles _ =
  let open G in
  G.load [kr; p1; p2; ke] [op; wr; fr1; fr2];
  TestApp.start ();
  G.close ()

let two_circles _ =
  let open G in
  G.load [kc; p1; p2; ke] [op; wc; fc1; fc2];
  TestApp.start ();
  G.close ()

let deleting_nothing _ =
  let open G in
  G.load [kd; p1; ke] [op; wd; cl; wd];
  TestApp.start ();
  G.close ()

let normal_suite =
  "Coreldro normal" >:::
  ["exit">::exit
  ;"circle">::circle
  ;"rectangle">::rectangle
  ;"two rectangles">::two_rectangles
  ;"two circles">::two_circles
  ]

let border_suite =
  "Coreldro border cases" >:::
  ["deleting nothing">::deleting_nothing]

let _ =
  run_test_tt_main normal_suite;
  run_test_tt_main border_suite
