open GraphicsIntf

module App (Gr: GRAPHICS) = struct
  open Gr
  (* We make an alias of the original Graphics module of OCaml
     in case we need to access it *)
  module G = Graphics

  let start () =
    try
      open_graph "Coreldro";
      () (* reemplazar el valor unitario () por su codigo *)
    with Graphic_failure _->()

end
