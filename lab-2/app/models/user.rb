require 'sinatra/activerecord'


class User < ActiveRecord::Base
  belongs_to :location

=begin
  validates :password, length: { in: 5..10, on: :create,
                        :message => ": your password must be 5-10 characters.\r\n" }
  validates :email, format: { with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/,
                      :message => ": please, introduce a valid email address.\r\n"}

=end

  def format_json
    h = self.as_json
    h["location"] = h.delete "location_id"
  end
end

class Provider < User
  has_many :orders
  has_many :dishes

=begin
  validates :store_name,
            :presence => {:message => ": please, choose a valid name for your delivery.\r\n"}
=end

end

class Consumer < User
  has_many :orders

  def as_json(options = {})
    options[:only] ||= self.class.readable_attributes
    super_json = super(options)
    super_json["location"] = super_json.delete "location_id"
    super_json
  end

  private
  def self.readable_attributes
    ["id", "email", "wallet", "location_id"]
  end
end
