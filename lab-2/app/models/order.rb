require 'sinatra/activerecord'


class Order < ActiveRecord::Base
  enum status: {payed: 0, delivered: 1, finished: 2}
  belongs_to :consumer, class_name: "Consumer"
  belongs_to :provider, class_name: "Provider"
  has_and_belongs_to_many :dishes

  def as_json(options = {})
  # Hardcoded method to parse model attrs with
  # json response
    super_json = super(options)
    super_json["provider_name"] = self.provider.store_name
    super_json["consumer_email"] = self.consumer.email
    super_json["consumer_location"] = self.consumer.location
    super_json["provider"] = super_json.delete "provider_id"
    super_json["consumer"] = super_json.delete "consumer_id"
    super_json["amount"] = calculate_amount
    super_json
  end

  def calculate_amount
  # Returns the total amount of the order
    quantity = self.dishes.group(:id).count
    amount = 0.0
    quantity.each do |id, quant|
      amount += self.dishes.find(id).price * quant
    end
    amount.to_f
  end
end
