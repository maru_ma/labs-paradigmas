class CreateDish < ActiveRecord::Migration[5.1]
  def change
    create_table :dishes do |t|
      t.string :name
      t.decimal :price, default: 0.0
      t.text :description

      t.integer :provider_id
    end

    create_table :dishes_orders, id: false do |t|
      t.integer :dish_id
      t.integer :order_id
    end
    add_index :dishes_orders, :dish_id
    add_index :dishes_orders, :order_id
  end
end
