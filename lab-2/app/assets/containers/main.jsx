import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import Home from '../presentational/home';
import NavBar from '../presentational/nav_bar';


export default class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      locations: [],
      selected_location: '',
      redirect_path: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value.toString();
    console.log(event.target.value);
    require('util').log(this.state);

    this.setState({
      selected_location: value
    });

    require('util').log(this.state);
  }

  handleSubmit(event) {
    console.log(event.target.value);
    const { selected_location } = this.state;
    const path = "/delivery/" + selected_location;
    console.log(path);
    console.log(this.state.redirect_path)
    console.log('' ? 'true' : 'false');
    this.setState({ redirect_path: path });
  }

  componentDidMount() {
    axios
      .get("/api/locations")
      .then(
        response => {
          this.setState({locations: response.data, loading: false});
          console.log(this.state.locations);
        }
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data && error.response.status !== 404)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({loading: false});
        }
      );
  }

  render() {
    const isLogged = sessionStorage.getItem('AuthUser') || false;
    const { redirect_path } = this.state;
    console.log(isLogged);
    if (redirect_path) {
      return (
        <Redirect push to={redirect_path} />
      );
    } else {
      return (
        <div>
        <NavBar isLogged={isLogged}/>
        <Home
          loading={this.state.loading}
          locations={this.state.locations}
          handle_change={this.handleChange}
          handle_submit={this.handleSubmit}
        />
        </div>
      );
    }
  }
}
