import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import PMenuProvider from '../presentational/menu_provider';

import NavBar from '../presentational/nav_bar';

export default class MenuProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      redirect: false,
      provider_id: parseInt(this.props.match.params.id),
      consumer_id: sessionStorage.getItem('user_id'),
      menu: [],
      order: [],
      total: 0.0,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);

    this.updateTotal = this.updateTotal.bind(this);
  }

  componentDidMount() {
    axios
      .get("/api/items", {
        params: {
          provider: this.state.provider_id,
        }
      }).then(
        res => this.setState({
          loading: false,
          menu: res.data
        })
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data &&
          error.response.status !== 400)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({ loading: false });
        }
      );
  }

  handleChange(event) {
    const id = event.target.id;
    let amount = Number(event.target.value);
    let temp_order = this.state.order;
    const ordLength = temp_order.length;
    let updated = false;
    if (amount >= 0) {
      for (var i = 0; !updated && i < ordLength; i++) {
        if (temp_order[i].id == id) {
          if (amount <= 0) {
            temp_order.splice(i, 1);
            updated = true;
          } else {
            temp_order[i].amount = amount;
            updated = true;
          }
        }
      }
    } else {
      return ( alert("No se pueden ingresar numeros negativos"));
    }

    if (!updated) {
      temp_order.push({id: id, amount: amount});
    }

    let temp_total = parseFloat(this.updateTotal(temp_order)).toFixed(2);

    console.log(this.state.total);
    this.setState({
      order: temp_order,
      total: temp_total,
    });
  }

  handleClick(event) {
    axios
      .post("/api/orders", {
          provider: this.state.provider_id,
          items: this.state.order,
          consumer: parseInt(sessionStorage.getItem('AuthUser')),
        }
      ).then(
        res => this.setState({ redirect: true })
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data && error.response.status !== 404)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({loading: false});
        }
      );
      event.preventDefault();
  }

  updateTotal(order) {
    const ordLength = order.length;
    let result = 0.0;
    const menu = this.state.menu;
    const lookUpPrice = function (id) {
      for (var i = 0; i<menu.length; i++) {
        if (menu[i].id == id) {
          return menu[i].price;
        }
      }
    };
    for (var i = 0; i < ordLength; i ++) {
      result = result + (order[i].amount * lookUpPrice(order[i].id));
    }
    return result;
  }

  render() {
    const isLogged = sessionStorage.getItem('AuthUser') || false;
    const { redirect } = this.state;
    if(redirect) {
      return (
        <Redirect to='/profile'/>
      );
    } else {
      return(
        <div>
        <NavBar isLogged={isLogged}/>
        <PMenuProvider
          loading={this.state.loading}
          menues={this.state.menu}
          handle_change={this.handleChange}
          handle_click={this.handleClick}
          total={this.state.total}
        />
        </div>
      );
    }
  }
}
