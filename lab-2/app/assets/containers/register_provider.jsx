import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import PRegisterProvider from '../presentational/register_provider';

import NavBar from '../presentational/nav_bar';


export default class RegisterProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      email: '',
      store_name: '',
      location: 0,
      password: '',
      locations: [],
      redirect: false,
      register: 0,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    axios
      .get("/api/locations")
      .then(
        response => {this.setState({locations: response.data, loading: false});
        console.log(this.state.locations);
      }
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data && error.response.status !== 404)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({loading: false});
        }
      );
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit() {
    axios
      .post("/api/providers", {
        //TODO: que pasa si mando this.state solamente
        email: this.state.email,
        store_name: this.state.store_name,
        location: this.state.location,
        password: this.state.password,
      }).then(
        this.setState({redirect: true, loading: false})
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data && error.response.status !== 404)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({loading: false});
        }
      );
  }

  render () {
    const isLogged = sessionStorage.getItem('AuthUser') || false;
    const { redirect } = this.state;
    if (redirect) {
      console.log('oaeu');
      return (<Redirect push to="/"/>);
    } else {
      require('util').log(this.state);
      console.log('else');
    return (
      <div>
        <NavBar isLogged={isLogged}/>
        <PRegisterProvider
          loading={this.state.loading}
          locations={this.state.locations}
          handle_change={this.handleChange}
          handle_submit={this.handleSubmit}
          state={this.state}
        />
      </div>
    );
  }
}}
