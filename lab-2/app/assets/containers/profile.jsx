import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import AccountInfo from '../presentational/account';
import ListOrders from '../presentational/profile';
import ListMenu from '../presentational/menu';
import NavBar from '../presentational/nav_bar';

export default class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      show: 0,
      email: sessionStorage.getItem('Email'),
      wallet: 0.0,
      user_id: parseInt(sessionStorage.getItem('AuthUser')),
      orders: [],
      isProvider: sessionStorage.getItem('AuthType'),
      items: [],
      newDishName: '',
      newDishPrice: 0.0,
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleDeliver = this.handleDeliver.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateItems = this.updateItems.bind(this);
  }

  componentDidMount() {
    const { user_id } = this.state;
    var that = this;
    console.log('montado');
    if(this.state.isProvider == 'provider') {
      axios.all([
        axios.get("/api/users/" + user_id),
        axios.get("/api/orders", {
          params: {
            user_id: user_id
          }
        }),
        axios.get("/api/items", {
          params: {
            provider: user_id
          }
        })
      ]).then(
        axios.spread(function (user_info, orders, items) {
          const wallet = user_info.data.wallet;
          that.setState({
            wallet: wallet,
            orders: orders.data,
            items: items.data,
            loading: false,
          });
        })
      ).catch(
        error => console.log(error)
      );
    } else {
      axios.all([
        axios.get("/api/users/" + user_id.toString()),
        axios.get("/api/orders", {
          params: {
            user_id: user_id
          }
        })
      ]).then(
        axios.spread(function (user_info, orders) {
          const ordersData = orders.data;
          const wallet = user_info.data.wallet;
          that.setState({
            wallet: wallet,
            orders: ordersData,
            loading: false,
          });
        })
      ).catch (
        error => console.log(error)
      );
    }
  }

  handleClick(event) {
    const showValue = parseInt(event.target.name);
    console.log(showValue);
    this.setState({
      show: showValue,
    });
  }

  handleDeliver(event) {
    console.log(event.target);
    const order_id = event.target.name;
    axios
      .post("/api/deliver/" + order_id).then(
        this.changeStatus(parseInt(order_id))
      ).catch(
        error => alert(error)
      );
  }

  changeStatus(orderId) {
    console.log("####################");
    let newOrders = this.state.orders;
    let ordLength = newOrders.length;
    for (var i = 0; i < ordLength; i ++) {
      if (newOrders[i].id == orderId)
        newOrders[i].status = 'delivered'
    }

    this.setState({
      orders: newOrders,
    })
  }

  handleSubmit(event) {
    axios
      .post("/api/items", {
          name: this.state.newDishName,
          price: this.state.newDishPrice,
          provider: this.state.user_id,
        }
      ).then(
        res => {
          let newDish = {id: res.data,
                        name: this.state.newDishName,
                          price: this.state.newDishPrice,
                          provider: this.state.user_id};
          let newItems = this.state.items;
          newItems.push(newDish);
        this.setState({
          newDishName: '',
          price: 0.0,
          items: newItems,
        })
      }).catch(
        error => console.log(error)
      );
    this.forceUpdate();
    event.preventDefault();
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name] : value
    });
  }

  updateItems() {
    axios
      .get("/api/items", {
        params: {
          provider: this.state.user_id,
        }
      }).then(
        res => this.setState({
          items: res.data,
          updateItems: false,
        })
      ).catch(
        error => console.log(error)
      );
  }

  render () {
    const isLogged = sessionStorage.getItem('AuthUser') || false;
    if (this.state.updateItems) {
      this.updateItems();
    }
    return (
      <div>
        <NavBar isLogged={isLogged}/>
        <AccountInfo
          loading={this.state.loading}
          email={this.state.email}
          wallet={this.state.wallet}
          handle_click={this.handleClick}
          isProvider={this.state.isProvider}
        />

        <ListOrders
          loading={this.state.loading}
          orders={this.state.orders}
          isProvider={this.state.isProvider}
          handle_deliver={this.handleDeliver}
          show={this.state.show}
        />
        {this.state.show === 2 &&
          <ListMenu
            loading={this.state.loading}
            items={this.state.items}
            handle_submit={this.handleSubmit}
            handle_delete={this.handleDelete}
            handle_change={this.handleChange}
          />}
      </div>
    );
  }
}
