import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import PLogin from '../presentational/login';


export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      email: '',
      password: '',
      id: '',
      provider: '',
      redirect: false,
      submited: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (this.state.submited) {
      sessionStorage.setItem(
        'AuthUser', this.state.id
      );
      sessionStorage.setItem(
        'AuthType', this.state.type
      );
      sessionStorage.setItem(
        'Email', this.state.email
      )
      this.setState({ redirect: true });
    } else {
      console.log('else');
      this.setState({ loading: false });
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    axios
      .post("/api/login", {
          email: this.state.email,
          password: this.state.password,
        }
      ).then(
        res => {
          const id = res.data.id.toString();
          const type = res.data.isProvider ? 'provider' : 'consumer';
          sessionStorage.setItem(
            'AuthUser', id
          );
          sessionStorage.setItem(
            'AuthType', type
          );
          sessionStorage.setItem(
            'Email', this.state.email
          );
          this.setState({
          id: id,
          redirect: true,
        })}
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data &&
          error.response.status == 401)
            alert('No existe el usuario');
          else if (error.response.data &&
          error.response.status == 403)
            alert('Password incorrecta');
          this.setState({ loading: false });
        }
      );
      this.forceUpdate();
      event.preventDefault();
  }

  render () {
    const isLogged = sessionStorage.getItem('AuthUser') || false;
    require('util').log(this.state);
    if (this.state.redirect) {
      return (
        <Redirect to="/" />
      );
    } else {
      return(
        <div>
        <NavBar isLogged={isLogged}/>
        <PLogin
          handle_submit={this.handleSubmit}
          handle_change={this.handleChange}
          loading={this.state.loading}
        />
        </div>
      );
    }
  }
}
