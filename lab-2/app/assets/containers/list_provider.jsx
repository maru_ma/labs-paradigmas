import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import PListProvider from '../presentational/list_provider';

import NavBar from '../presentational/nav_bar';

export default class ListProvider extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      providers: [],
      location: this.props.match.params.id,
    };
  }

  componentDidMount() {
    axios
      .get(
        "/api/providers", {
          params:{
            location: this.state.location,
          }
        }
      ).then(
        response => this.setState({
          loading: false,
          providers: response.data
        })
      ).catch(
        error => {
          if (!error.response)
            alert(error);
          else if (error.response.data && error.response.status !== 404)
            alert(error.response.data);
          else
            alert(error.response.statusText);
          this.setState({loading: false});
        }
      );
  }

  render () {
    const isLogged = sessionStorage.getItem('AuthUser') || false;
    console.log(this.state.providers);
    return (
      <div>
        <NavBar isLogged={isLogged}/>
        <PListProvider
          providers={this.state.providers}
          loading={this.state.loading}
        />
      </div>
    );
  }
}
