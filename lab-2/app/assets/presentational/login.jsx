import React from 'react';
import PropTypes from 'prop-types';

export default function PLogin(props) {
  let { handle_submit, loading, handle_change } = props;

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
          <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <form>
        <div>
          <label>Correo electronico</label>
          <input
            type='email'
            name='email'
            onChange={(e) => handle_change(e)}
          />
        </div>
        <div>
          <label>Password</label>
          <input
            type='password'
            name='password'
            onChange={(e) => handle_change(e)}
          />
        </div>
        <button
          type='submit'
          onClick={(e) => handle_submit(e)}
        >Log in</button>
      </form>
    );
  }
}
