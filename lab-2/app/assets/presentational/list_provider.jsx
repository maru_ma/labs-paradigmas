import React from 'react';
import PropTypes from 'prop-types';

export default function PListProvider(props) {
  let { providers, loading } = props;

  function listProvider(provider) {
    const href = "/menu/" + provider.id.toString();
    return (
      <a href={href}>{provider.store_name}</a>
    );

  }

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
          <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
      if (!providers.length) {
        return(
          <p>
            No se encontraron deliveries en tu zona
          </p>
        );
      } else {
        return (
          <div>
            <ul>
              {providers.map((provider) =>
                <li key={provider.id.toString()}>
                  {listProvider(provider)}
                </li>
              )}
            </ul>
          </div>
        );
      }
  }
}
