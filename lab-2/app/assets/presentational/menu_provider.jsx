import React from 'react';
import PropTypes from 'prop-types';

export default function PMenuProvider(props) {
  let { loading, menues, handle_change, handle_click, total } = props;

  if(loading){
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
          <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return(
      <div>
        {menues.map((menu) =>
          <div key={menu.id.toString()}>
            <p>Nombre: {menu.name}</p>
            <p>Precio: ${menu.price}</p>
            <input
              type='number'
              min='0'
              id={menu.id}
              onChange={(e) => handle_change(e)}
            />
          </div>
        )}
        <p>
          <strong>Total: </strong>${total}
        </p>
        <button
          type='submit'
          onClick={(e) => handle_click(e)}
        >
          Realizar pedido
        </button>
      </div>
    );
  }
}
