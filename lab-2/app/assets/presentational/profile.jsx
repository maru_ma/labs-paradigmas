import React from 'react';
import PropTypes from 'prop-types';

export default function ListOrders(props) {
  let { loading, orders, isProvider, handle_deliver, show } = props;

  function displayOrder(order) {
    return (
      <div>
        <div>
          <h5>{order.provider_name}: ${order.amount}</h5>
          <span>Status: {order.status}</span>
        </div>
      </div>
    );
  }

  function displayOrderProvider(order) {
    return (
      <div>
        <div>
          <h5><strong>{order.consumer_email}: ${order.amount}</strong></h5>
          <p> Estado: {order.status} </p>
        {order.status == 'payed' &&
          <button type='button' name={order.id.toString()} onClick={(event) => handle_deliver(event, order.id)}>
            Pedido Enviado
          </button>
        }
        </div>
      </div>
    );
  }

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
        <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    if (isProvider !== 'provider') {
      return (
        <div>
          {show === 1 && orders.map((order) =>
            <div key={order.id.toString()}>
              {displayOrder(order)}
            </div>
            )}
        </div>
      );
    } else {
      return (
        <div>
          {show === 1 && orders.map((order) =>
            <div key={order.id.toString()}>
              {displayOrderProvider(order)}
            </div>
          )}
        </div>
      );
    }
  }
}
