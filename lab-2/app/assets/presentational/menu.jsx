import React from 'react';
import PropTypes from 'prop-types';

export default function ListMenu(props) {
  let { loading, items, handle_submit, handle_delete, handle_change } = props;

  return (
    <div>
    <div>
      {items.map((item) =>
        <div key={item.id.toString()}>
          <p>Nombre: {item.name}</p>
          <p>Precio: ${item.price}</p>
          <button type="button"
            onClick={(e) => handle_delete(e)}
            name={item.id.toString()}>
            Eliminar plato
          </button>
        </div>
        )}
    </div>
    <div>
    <form>
      <div>
      <label>Nuevo plato</label>
      <input
        type="text"
        name="newDishName"
        onChange={(e) => handle_change(e)}
      />
      <input
        type="number"
        min="0"
        step="0.5"
        name="newDishPrice"
        onChange={(e) => handle_change(e)}
      />
      <button type="submit" onClick={(e) => handle_submit(e)}>
        Agregar plato
      </button>
      </div>
    </form>
    </div>
    </div>
  );
}
