import React from 'react';
import PropTypes from 'prop-types';

export default function NavBar(props) {
  let { isLogged } = props;

  return(
    <div>
      {isLogged &&
        <nav>
        <div>
        <a href='/'>Deliveru</a>
        </div>
        <div>
        <a href='/profile'>Profile</a>
        </div>
        <div>
        <a href='/logout'>Log out</a>
        </div>
        </nav>
      }
      {isLogged == false &&
        <nav>
        <div>
        <a href='/'>Deliveru</a>
        </div>
        <div>
        <a href='/login'>Log in</a>
        </div>
        <div>
        <a href='/signup'>Sign up</a>
        </div>
        </nav>
      }
    </div>
  );
}
