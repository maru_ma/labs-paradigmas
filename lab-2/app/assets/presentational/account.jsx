import React from 'react';
import PropTypes from 'prop-types';

export default function AccountInfo(props) {
  let { loading, email, wallet, handle_click, isProvider } = props;

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
          <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <div>
      <div>
        <ul>
          <li><strong>Correo electronico</strong> {email}</li>
          <li><strong>Balance</strong> ${wallet}</li>
        </ul>
      </div>
      <div>
        <button type="button" name="1" onClick={(e) => handle_click(e)}>
          Ver Pedidos
        </button>
        {isProvider == 'provider' &&
          <button type="button" name="2" onClick={(e) => handle_click(e)}>
            Ver Menu
          </button>
        }
      </div>
      </div>
    );
  }
}
