import React from 'react';
import PropTypes from 'prop-types';

export default function PRegisterProvider(props) {
  let { loading, locations, handle_change, handle_submit, state, change_form } = props;


  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
          <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <form>
      <div>
      <label>
        Nombre de delivery
        <input
          name='store_name'
          type='text'
          value={state.store_name}
          onChange={(e) => handle_change(e)}
        />
      </label>
      </div>
      <div>
      <label>
        Correo elertronico
        <input
          name='email'
          type='text'
          value={state.email}
          onChange={(e) => handle_change(e)}
        />
      </label>
      </div>
      <div>
      <label>
        Contrasena
        <input
          name='password'
          type='password'
          value={state.password}
          onChange={(e) => handle_change(e)}
        />
      </label>
      </div>
      <div>
      <label>
        Ubicacion
        <select name='location' type='select' onChange={(e) => handle_change(e)}>
          <option selected='selected'></option>
          { locations.map((loc) => (
            <option key={loc.id.toString()} value={ loc.id }>
              { loc.name }
            </option>
          ))}
        </select>
      </label>
      </div>
      <div>
      <button type='submit' onClick={() => handle_submit() }>Registrarse</button>
      </div>
      </form>
    );
  }
}
