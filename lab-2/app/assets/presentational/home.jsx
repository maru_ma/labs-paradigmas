import React from 'react';
import PropTypes from 'prop-types';

export default function Home(props) {
  let { loading, locations, handle_change, handle_submit } = props;

  if (loading) {
    return (
      <div className="body text-center my-3">
        <div className="fa-3x my-3">
        <i className="fas fa-spinner fa-spin"/>
        </div>
        <h4>Loading...</h4>
      </div>
    );
  } else {
    return (
      <div className="body">
        <div className="main-div">
          <h1 className="display-3 text-center mb-5">
            ¿Qu&eacute; vas a comer hoy?</h1>
          <select name='select' onChange={(event) => handle_change(event)}>
            <option selected='selected'></option>
            { locations.map((loc) => (
                <option key={loc.id.toString()}
                    value={loc.id}>{ loc.name }</option>
              ))
            }
          </select>
          <input type="button" value='btn' onClick={(e) => handle_submit(e)}/>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  loading: PropTypes.bool.isRequired,
  locations: PropTypes.array.isRequired
};
