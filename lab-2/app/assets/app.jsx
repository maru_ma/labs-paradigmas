import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from './containers/main';
import NotFound from './presentational/notfound';
import RegisterProvider from './containers/register_provider';
import PRegisterProvider from './presentational/register_provider';

import ListProvider from './containers/list_provider';
import MenuProvider from './containers/menu_provider';
import Login from './containers/login';
import Profile from './containers/profile';

export default function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/profile' component={Profile}/>
        <Route exact path='/signup' component={RegisterProvider} />
        <Route path='/delivery/:id' component={ListProvider}/>
        <Route exact path='/menu/:id' component={MenuProvider}/>
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
}
